FROM debian:latest
FROM ubuntu:latest
LABEL key="Mustafif Khan"

RUN apt-get update
RUN apt-get -y install wget
RUN wget https://github.com/MKProj/MKProj/raw/gh-pages/Packages/phaktionz-wiki_1.0.1_amd64.deb
RUN echo 'First install attempt'
RUN apt-get install phaktionz-wiki_1.0.1_amd64.deb
RUN apt-get install -f
RUN echo 'Final install attempt'
RUN apt-get install phaktionz-wiki_1.0.1_amd64.deb
RUN echo 'Make sure of installation: apt-cache search phaktionz-wiki'
RUN echo 'Any issues contact Mustafif@mkproj.com'